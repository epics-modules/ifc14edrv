#ifndef IFC14DEVICE_H
#define IFC14DEVICE_H

#include <nds3/nds.h>

#include <ifcdaqdrv.h>

#include "IFC14AIChannelGroup.h"
#include "IFC14AIChannel.h"

class IFC14Device
{
  public:
    IFC14Device(nds::Factory& factory, const std::string &deviceName, const nds::namedParameters_t& parameters);
    ~IFC14Device();

  private:
    std::vector<std::shared_ptr<IFC14AIChannelGroup> > m_AIChannelGroups;
    struct ifcdaqdrv_usr m_deviceUser;
    nds::Node m_node;
};

#endif /* IFC14DEVICE_H */
