
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>

#include <nds3/nds.h>
#include <ifcdaqdrv.h>

#include "IFC14.h"
#include "IFC14Device.h"
#include "IFC14AIChannelGroup.h"

IFC14Device::IFC14Device(nds::Factory& factory, const std::string &deviceName, const nds::namedParameters_t& parameters) :
        m_node(deviceName) {
    ifcdaqdrv_status status;

    m_deviceUser = {static_cast<uint32_t>(std::stoul(parameters.at("card"))),
                    static_cast<uint32_t>(std::stoul(parameters.at("fmc"))),
                    NULL};

    status = ifcdaqdrv_open_device(&m_deviceUser);
    if(status != status_success) {
        throw nds::NdsError("Failed to open device");
    }
    status = ifcdaqdrv_init_adc(&m_deviceUser);
    if(status != status_success) {
        throw nds::NdsError("Failed to initialize ADC");
    }

    std::shared_ptr<IFC14AIChannelGroup> aichgrp = std::make_shared<IFC14AIChannelGroup>("AI", m_node, m_deviceUser);
    m_AIChannelGroups.push_back(aichgrp);

    // Initialize device
    m_node.initialize(this, factory);
}

IFC14Device::~IFC14Device() {
    ifcdaqdrv_close_device(&m_deviceUser);
}

NDS_DEFINE_DRIVER(ifc14, IFC14Device);
